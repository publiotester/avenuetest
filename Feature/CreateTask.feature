Feature: Create Task
 
Scenario: Validate the link of page My Tasks
	Given User is on Home Page
	When User see 'My Tasks' link on the NavBar
	And User click on the link 'My Tasks'
	Then Application move to 'My Tasks' page showing a message on the top
 
Scenario: Create new task by hitting enter
	Given User is on Home Page
	When User see 'My Tasks' link on the NavBar
	And User click on the link 'My Tasks'
	And User enters description of a new task
	And User hits enter
	Then Application inserts new task appended on the list of created tasks
	
Scenario: Create new task clicking on the add task button
	Given User is on Home Page
	When User see 'My Tasks' link on the NavBar
	And User click on the link 'My Tasks'
	And User enters description of a new task
	And User clicks on the add task button
	Then Application inserts new task appended on the list of created tasks

Scenario: Validate minimum number of characters
	Given User is on Home Page
	When User see 'My Tasks' link on the NavBar
	And User click on the link 'My Tasks'
	And User enters description with less than three characters 
	And User clicks on the add task button
	Then Application doesn't enable button to insert new task
	
Scenario: Validate maximum number of characters
	Given User is on Home Page
	When User see 'My Tasks' link on the NavBar
	And User click on the link 'My Tasks'
	And User enters description with two hundred and fifty one characters
	Then Application only allows the insertion of two hundred and fifty characters