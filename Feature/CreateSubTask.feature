Feature: Create SubTask
 
Scenario: Validate button of Subtasks modal
	Given User is on Home Page
	When User see 'My Tasks' link on the NavBar
	And User click on the link 'My Tasks'
	And User see a button labeled as 'Manage Subtasks'
	And User see the number of subtasks on the label
	And User click on the button 'Manage Subtasks'
	Then Application open the modal of 'Subtasks'
 
Scenario: Validate read only field of task
	Given User is on Home Page
	When User see 'My Tasks' link on the NavBar
	And User click on the link 'My Tasks'
	And User see a button labeled as 'Manage Subtasks'
	And User click on the button 'Manage Subtasks'
	Then Application open the modal with read only field (task ID and task description)
	
Scenario: Validate maximum number of characters
	Given User is on Home Page
	When User see 'My Tasks' link on the NavBar
	And User click on the link 'My Tasks'
	And User see a button labeled as 'Manage Subtasks'
	And User click on the button 'Manage Subtasks'
	And User enters subtask description with two hundred and fifty one characters
	Then Application only allows the insertion of subtask with two hundred and fifty characters

Scenario: Validate date format in the field SubTask due date
	Given User is on Home Page
	When User see 'My Tasks' link on the NavBar
	And User click on the link 'My Tasks'
	And User see a button labeled as 'Manage Subtasks'
	And User click on the button 'Manage Subtasks'
	And User enters description of a new subtask
	And User enters due date with format different of (MM/dd/yyyy)
	And User clicks on the add button
	Then Application doesn't enable button to insert new subtask

Scenario: Validate required fields
	Given User is on Home Page
	When User see 'My Tasks' link on the NavBar
	And User click on the link 'My Tasks'
	And User see a button labeled as 'Manage Subtasks'
	And User click on the button 'Manage Subtasks'
	And User clicks on the add button without filling in the fields
	Then Application doesn't enable button to insert new subtask

Scenario: Create new subtask clicking on the add button
	Given User is on Home Page
	When User see 'My Tasks' link on the NavBar
	And User click on the link 'My Tasks'
	And User see a button labeled as 'Manage Subtasks'	
	And User click on the button 'Manage Subtasks'
	And User enters description of a new subtask
	And User enters due date with valid format (MM/dd/yyyy)
	And User clicks on the add button
	Then Application inserts new subtask appended on the bottom part of the modal dialog