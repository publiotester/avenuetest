package objects;

public class CreateSubTasksObjects {

	public static String titModalSubtasks = "/html/body/div[4]/div/div/div[1]/h3";
	public static String tbxTask = "edit_task";
	
	public static String tbxNewSubTask = "new_sub_task";
	public static String tbxDueDateSubTask = "dueDate";
	public static String btnAddSubTask = "add-subtask";

}
