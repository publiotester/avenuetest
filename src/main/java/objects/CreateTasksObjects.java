package objects;

public class CreateTasksObjects {

	public static String titMyTasks = "/html/body/div[1]/h1";

	public static String txbNewTask = "new_task";
	public static String btnIncludeTask = "/html/body/div[1]/div[2]/div[1]/form/div[2]/span";

	public static String btnManageSubTask = "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[4]/button";
}
