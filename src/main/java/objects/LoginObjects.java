package objects;

public class LoginObjects {

	public static String btnLogin = "/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a";

	public static String tbxEmail = "user_email";
	public static String tbxPassword = "user_password";
	public static String btnSignIn = "//*[@id='new_user']/input";

	public static String msgSuccessfulLogin = "/html/body/div[1]/div[2]";

}
