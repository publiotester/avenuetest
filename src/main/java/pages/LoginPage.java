package pages;

import static org.testng.Assert.assertEquals;
import objects.LoginObjects;
import objects.Parameters;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class LoginPage {

	WebDriver driver;
	
	public LoginPage(WebDriver driver){
		this.driver = driver;
	}
	
	public HomePage successfulLogin () {
		try {
			WebElement btnLogin = driver.findElement(By.xpath(LoginObjects.btnLogin));
			btnLogin.click();
			Thread.sleep(2000);
			WebElement tbxEmail = driver.findElement(By.id(LoginObjects.tbxEmail));
			tbxEmail.sendKeys(Parameters.email);
			WebElement tbxPassword = driver.findElement(By.id(LoginObjects.tbxPassword));
			tbxPassword.sendKeys(Parameters.password);		
			WebElement btnSignIn = driver.findElement(By.xpath(LoginObjects.btnSignIn));
			btnSignIn.click();
			assertEquals(driver.findElement(By.xpath(LoginObjects.msgSuccessfulLogin)).getText(), "Signed in successfully.");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return new HomePage(driver);
	}
}
