package pages;

import java.util.concurrent.TimeUnit;

import objects.Parameters;

import org.openqa.selenium.WebDriver;

public class ConfigDriver {

	WebDriver driver;

	public ConfigDriver(WebDriver driver) {
		this.driver = driver;
	}

	public LoginPage accessApplication() {
		driver.manage().window().maximize();
		int implicitWaitTime = (10);
		driver.manage().timeouts()
				.implicitlyWait(implicitWaitTime, TimeUnit.SECONDS);
		driver.get(Parameters.url);

		return new LoginPage(driver);
	}

	public void closeDriver() {
		try {
			Thread.sleep(3000);
			driver.close();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
