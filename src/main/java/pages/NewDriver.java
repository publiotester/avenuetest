package pages;

import objects.Parameters;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class NewDriver {

	WebDriver driver;

	public WebDriver createDriver(String browser) {
		
		try {
			if (browser.equals("Firefox")) {
				// If value of the parameter is Firefox, this will execute
				System.setProperty("webdriver.gecko.driver", Parameters.localPath + "\\geckodriver.exe");
				DesiredCapabilities dc = DesiredCapabilities.firefox();
				dc.setCapability("marionette", true);
				driver = new FirefoxDriver(dc);
			} else if (browser.equals("IE")) {
				// If value of the parameter is IE, this will execute
				System.setProperty("webdriver.ie.driver", Parameters.localPath + "\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			} else if (browser.equals("Chrome")) {
				// If value of the parameter is Chrome, this will execute
				System.setProperty("webdriver.chrome.driver", Parameters.localPath + "\\chromedriver.exe");
				driver = new ChromeDriver();
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return driver;
	}

}
