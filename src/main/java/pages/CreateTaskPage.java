package pages;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import objects.CreateTasksObjects;
import objects.Parameters;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CreateTaskPage {

	WebDriver driver;

	public CreateTaskPage(WebDriver driver) {
		this.driver = driver;
	}

	public CreateSubTaskPage clickManageSubtasks() {
		try {
			WebElement btnManageSubTask = driver.findElement(By
					.xpath(CreateTasksObjects.btnManageSubTask));
			btnManageSubTask.click();
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return new CreateSubTaskPage(driver);
	}

	public void validateRedirectionPage() {
		WebElement titMyTasks = driver.findElement(By
				.xpath(CreateTasksObjects.titMyTasks));
		assertEquals(titMyTasks.getText(), "Hey " + Parameters.namePerfil
				+ ", this is your todo list for today:");
	}

	public void includeNewTask() {
		WebElement txbNewTask = driver.findElement(By
				.id(CreateTasksObjects.txbNewTask));
		txbNewTask.sendKeys("New Task 1");
	}

	public void hitEnterToNewTask() {
		WebElement txbNewTask = driver.findElement(By
				.id(CreateTasksObjects.txbNewTask));
		txbNewTask.sendKeys(Keys.ENTER);
	}

	public void clickAddButtonToNewTask() {
		WebElement btnIncludeTask = driver.findElement(By
				.xpath(CreateTasksObjects.btnIncludeTask));
		btnIncludeTask.click();
	}

	public void validateNewTask() {
		assertTrue(driver.getPageSource().contains("New Task 1") == true);
	}
	
	public void includeNewTaskLessThreeChar() {
		WebElement txbNewTask = driver.findElement(By
				.id(CreateTasksObjects.txbNewTask));
		txbNewTask.sendKeys("Nt");
	}
	
	public void validateAddButtonNotEnable() {
		WebElement btnIncludeTask = driver.findElement(By
				.xpath(CreateTasksObjects.btnIncludeTask));
		assertTrue(!btnIncludeTask.isEnabled());
		assertTrue(driver.getPageSource().contains("Nt") == false);
	}
	
	public void includeNewTaskMoreTwoHundFiftChar() {
		WebElement txbNewTask = driver.findElement(By
				.id(CreateTasksObjects.txbNewTask));
		txbNewTask.sendKeys("012345678 012345678 012345678 012345678 012345678 "
				+ "012345678 012345678 012345678 012345678 012345678 "
				+ "012345678 012345678 012345678 012345678 012345678 "
				+ "012345678 012345678 012345678 012345678 012345678 "
				+ "012345678 012345678 012345678 012345678 012345678 1");
	}
	
	public void validateFieldWithNoMoreTwoHundFiftChar() {
		WebElement txbNewTask = driver.findElement(By
				.id(CreateTasksObjects.txbNewTask));
		int numberChar = txbNewTask.getAttribute("value").length();
		assertEquals(numberChar, 250);
	}
	
	public void validateButtonLabel() {
		WebElement btnManageSubTask = driver.findElement(By
				.xpath(CreateTasksObjects.btnManageSubTask));
		assertTrue(btnManageSubTask.getText().contains("Manage Subtasks"));
	}
	
	public void validateNumberSubTasksInButtonLabel() {
		WebElement btnManageSubTask = driver.findElement(By
				.xpath(CreateTasksObjects.btnManageSubTask));
		assertTrue(btnManageSubTask.getText().matches(".*\\d+.*"));
	}
}
