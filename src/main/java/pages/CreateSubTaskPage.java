package pages;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import objects.CreateSubTasksObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CreateSubTaskPage {

	WebDriver driver;
	
	public CreateSubTaskPage(WebDriver driver){
		this.driver = driver;
	}

	public void validateModalOpened() {
		WebElement titModalSubtasks = driver.findElement(By
				.xpath(CreateSubTasksObjects.titModalSubtasks));
		assertTrue(titModalSubtasks.getText().contains("Editing"));
	}
	
	public void validateFieldTaskReadOnly() {
		WebElement tbxTask = driver.findElement(By
				.id(CreateSubTasksObjects.tbxTask));
		assertTrue(!tbxTask.isEnabled());
	}
	
	public void includeNewSubTask() {
		WebElement tbxNewSubTask = driver.findElement(By
				.id(CreateSubTasksObjects.tbxNewSubTask));
		tbxNewSubTask.sendKeys("New Subtask 1");
	}
	
	public void includeInvalidDueDateSubTask() {
		WebElement tbxDueDateSubTask = driver.findElement(By
				.id(CreateSubTasksObjects.tbxDueDateSubTask));
		tbxDueDateSubTask.sendKeys("2018/01/01");
	}
	
	public void clickAddButton() {
		WebElement btnAddSubTask = driver.findElement(By
				.id(CreateSubTasksObjects.btnAddSubTask));
		btnAddSubTask.click();
	}
	
	public void validateAddButtonNotEnable() {
		WebElement btnAddSubTask = driver.findElement(By
				.id(CreateSubTasksObjects.btnAddSubTask));
		assertTrue(!btnAddSubTask.isEnabled());
		assertTrue(driver.getPageSource().contains("New Subtask 1") == false);
	}
	
	public void validateNewSubTask() {
		assertTrue(driver.getPageSource().contains("New Subtask 1") == true);
	}
	
	public void includeValidDueDateSubTask() {
		WebElement tbxDueDateSubTask = driver.findElement(By
				.id(CreateSubTasksObjects.tbxDueDateSubTask));
		tbxDueDateSubTask.sendKeys("01/01/2018");
	}
	
	public void includeNewSubTaskMoreTwoHundFiftChar() {
		WebElement tbxNewSubTask = driver.findElement(By
				.id(CreateSubTasksObjects.tbxNewSubTask));
		tbxNewSubTask.sendKeys("012345678 012345678 012345678 012345678 012345678 "
				+ "012345678 012345678 012345678 012345678 012345678 "
				+ "012345678 012345678 012345678 012345678 012345678 "
				+ "012345678 012345678 012345678 012345678 012345678 "
				+ "012345678 012345678 012345678 012345678 012345678 1");
	}
	
	public void validateFieldWithNoMoreTwoHundFiftChar() {
		WebElement tbxNewSubTask = driver.findElement(By
				.id(CreateSubTasksObjects.tbxNewSubTask));
		int numberChar = tbxNewSubTask.getAttribute("value").length();
		assertEquals(numberChar, 250);
	}
}
