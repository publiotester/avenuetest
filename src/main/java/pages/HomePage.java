package pages;

import static org.testng.Assert.assertEquals;
import objects.HomeObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

	WebDriver driver;

	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	public CreateTaskPage accessLinkMyTasks() {
		try {
			WebElement lnkMyTasks = driver.findElement(By
					.xpath(HomeObjects.lnkMyTasks));
			lnkMyTasks.click();
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return new CreateTaskPage(driver);
	}

	public void validateTextLinkMyTasks() {
		WebElement lnkMyTasks = driver.findElement(By
				.xpath(HomeObjects.lnkMyTasks));
		assertEquals(lnkMyTasks.getText(), "My Tasks");
	}
}
