package cucumberTest;

import objects.Parameters;

import org.openqa.selenium.WebDriver;

import pages.ConfigDriver;
import pages.CreateSubTaskPage;
import pages.CreateTaskPage;
import pages.HomePage;
import pages.NewDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ImplementationTest {

	private ConfigDriver configDriver;
	private HomePage homePage;
	private CreateTaskPage createTaskPage;
	private CreateSubTaskPage createSubTaskPage;
	private WebDriver driver = new NewDriver().createDriver(Parameters.browser);
	
	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {	
		configDriver = new ConfigDriver(driver);
		configDriver.accessApplication().successfulLogin();
	}

	@When("^User see 'My Tasks' link on the NavBar$")
	public void user_see_My_Tasks_link_on_the_NavBar() throws Throwable {
		homePage = new HomePage(driver);
		homePage.validateTextLinkMyTasks();
	}

	@When("^User click on the link 'My Tasks'$")
	public void user_click_on_the_link_My_Tasks() throws Throwable {
		homePage.accessLinkMyTasks();
	}

	@Then("^Application move to 'My Tasks' page showing a message on the top$")
	public void application_move_to_My_Tasks_page_showing_a_message_on_the_top() throws Throwable {
		createTaskPage = new CreateTaskPage(driver);
		createTaskPage.validateRedirectionPage();
		configDriver.closeDriver();
	}

	@When("^User enters description of a new task$")
	public void user_enters_description_of_a_new_task() throws Throwable {
		createTaskPage = new CreateTaskPage(driver);
		createTaskPage.includeNewTask();
	}

	@When("^User hits enter$")
	public void user_hits_enter() throws Throwable {
		createTaskPage.hitEnterToNewTask();
	}

	@Then("^Application inserts new task appended on the list of created tasks$")
	public void application_inserts_new_task_appended_on_the_list_of_created_tasks() throws Throwable {
		createTaskPage.validateNewTask();
		configDriver.closeDriver();
	}

	@When("^User clicks on the add task button$")
	public void user_clicks_on_the_add_task_button() throws Throwable {
		createTaskPage.clickAddButtonToNewTask();
	}

	@When("^User enters description with less than three characters$")
	public void user_enters_description_with_less_than_three_characters() throws Throwable {
		createTaskPage = new CreateTaskPage(driver);
		createTaskPage.includeNewTaskLessThreeChar();
	}
	
	@Then("^Application doesn't enable button to insert new task$")
	public void application_doesn_t_enable_button_to_insert_new_task() throws Throwable {
		createTaskPage.validateAddButtonNotEnable();
		configDriver.closeDriver();
	}

	@When("^User enters description with two hundred and fifty one characters$")
	public void user_enters_description_with_two_hundred_and_fifty_one_characters() throws Throwable {
		createTaskPage = new CreateTaskPage(driver);
		createTaskPage.includeNewTaskMoreTwoHundFiftChar();
	}

	@Then("^Application only allows the insertion of two hundred and fifty characters$")
	public void application_only_allows_the_insertion_of_two_hundred_and_fifty_characters() throws Throwable {
		createTaskPage.validateFieldWithNoMoreTwoHundFiftChar();
		configDriver.closeDriver();
	}

	@When("^User see a button labeled as 'Manage Subtasks'$")
	public void user_see_a_button_labeled_as_Manage_Subtasks() throws Throwable {
		createTaskPage = new CreateTaskPage(driver);
		createTaskPage.validateButtonLabel();
	}

	@When("^User see the number of subtasks on the label$")
	public void user_see_the_number_of_subtasks_on_the_label() throws Throwable {
		createTaskPage.validateNumberSubTasksInButtonLabel();
	}

	@When("^User click on the button 'Manage Subtasks'$")
	public void user_click_on_the_button_Manage_Subtasks() throws Throwable {
		createTaskPage.clickManageSubtasks();
	}

	@Then("^Application open the modal of 'Subtasks'$")
	public void application_open_the_modal_of_Subtasks() throws Throwable {
		createSubTaskPage = new CreateSubTaskPage(driver);
		createSubTaskPage.validateModalOpened();
		configDriver.closeDriver();
	}

	@Then("^Application open the modal with read only field \\(task ID and task description\\)$")
	public void application_open_the_modal_with_read_only_field_task_ID_and_task_description()
			throws Throwable {
		createSubTaskPage = new CreateSubTaskPage(driver);
		createSubTaskPage.validateFieldTaskReadOnly();
		configDriver.closeDriver();
	}

	@When("^User enters description of a new subtask$")
	public void user_enters_description_of_a_new_subtask() throws Throwable {
		createSubTaskPage = new CreateSubTaskPage(driver);
		createSubTaskPage.includeNewSubTask();
	}

	@When("^User enters due date with format different of \\(MM/dd/yyyy\\)$")
	public void user_enters_due_date_with_format_different_of_MM_dd_yyyy()
			throws Throwable {
		createSubTaskPage.includeInvalidDueDateSubTask();
	}

	@When("^User clicks on the add button$")
	public void user_clicks_on_the_add_button() throws Throwable {
		createSubTaskPage.clickAddButton();
	}

	@Then("^Application doesn't enable button to insert new subtask$")
	public void application_doesn_t_enable_button_to_insert_new_subtask()
			throws Throwable {
		createSubTaskPage.validateAddButtonNotEnable();
		configDriver.closeDriver();
	}

	@When("^User clicks on the add button without filling in the fields$")
	public void user_clicks_on_the_add_button_without_filling_in_the_fields()
			throws Throwable {
		createSubTaskPage = new CreateSubTaskPage(driver);
		createSubTaskPage.clickAddButton();
	}

	@Then("^Application inserts new subtask appended on the bottom part of the modal dialog$")
	public void application_inserts_new_subtask_appended_on_the_bottom_part_of_the_modal_dialog()
			throws Throwable {
		createSubTaskPage.validateNewSubTask();
		configDriver.closeDriver();
	}

	@When("^User enters due date with valid format \\(MM/dd/yyyy\\)$")
	public void user_enters_due_date_with_valid_format_MM_dd_yyyy()
			throws Throwable {
		createSubTaskPage.includeValidDueDateSubTask();
	}
	
	@When("^User enters subtask description with two hundred and fifty one characters$")
	public void user_enters_subtask_description_with_two_hundred_and_fifty_one_characters() throws Throwable {
		createSubTaskPage = new CreateSubTaskPage(driver);
		createSubTaskPage.includeNewSubTaskMoreTwoHundFiftChar();
	}

	@Then("^Application only allows the insertion of subtask with two hundred and fifty characters$")
	public void application_only_allows_the_insertion_of_subtask_with_two_hundred_and_fifty_characters() throws Throwable {
		createSubTaskPage.validateFieldWithNoMoreTwoHundFiftChar();
		configDriver.closeDriver();
	}	
}